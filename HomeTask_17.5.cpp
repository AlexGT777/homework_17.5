﻿// HomeTask_17.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>
using namespace std;

// объявление класса
class Vector 
{
private: // спецификатор доступа private
    double x, // координата х Вектора 
           y, // координата у Вектора 
           z; // координата z Вектора 

    double modul = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)); // модуль Вектора
    
public: // спецификатор доступа public
    
    Vector() : x(0), y(0), z(0)  // конструктор класса по умолчанию
    {}
    
    Vector(double x_coordinate, double y_coordinate, double z_coordinate) : x(x_coordinate), y(y_coordinate), z(z_coordinate)// инициализируем элементы
    {}

    void message() // функция (метод класса) выводящая сообщение на экран
    {
        cout << "Coordinate of the Vector A is: \n";
    }
    
    void ShowCoordinate() // отобразить координаты Вектора
    {
        cout << "\nx = " << x << "\ny = " << y << "\nz = " << z << endl;
    }

    void SnowModul() // отобразить координаты Вектора
    {
        cout << "\nModul Vector |A| = " << modul << endl;
    }
};


int main()
{
    Vector NewVector (6, 2, 3); // объявляем имя Вектора и его координаты
    NewVector.message(); // вызываем функцию message
    NewVector.ShowCoordinate(); // отображаем координаты Вектор через метод ShowCoordinate()
    NewVector.SnowModul(); // отображаем модуль Вектора через метод SnowModul()

    return 0;
}

